c = get_config()
# increase timeout to 120 seconds
c.Execute.timeout = 600
c.ClearSolutions.code_stub = { "julia": "# your code here" }
c.CourseDirectory.course_id = "cse6643"
#c.Exchange.root = "/tmp/exchange"
c.CourseDirectory.submitted_directory = 'submitted'
c.ZipCollectApp.strict = True
c.FileNameCollectorPlugin.named_regexp = (
    '.+/(?P<student_id>[a-z]+)'
    '_*'
    '[A-Z]*'
    '_[0-9]+_[0-9]+_'
    '(?P<file_id>[a-z\-]*)'
    '\.ipynb'
)
gtids = [
        #TODO: list of GT user names
        ]
c.CourseDirectory.db_students = [dict(id=canvasname, first_name=canvasname, last_name="") for canvasname in gtids]

c.FileNameCollectorPlugin.named_regexp = (
    '.+/(?P<student_id>[a-z]+)'
    '.*'
    '\.ipynb'
)
