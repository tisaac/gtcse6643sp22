# use like 
#
#  nbgrader zip_collect hw5 --force --collector=cse6643collector.CSE6643CollectorHW5

from nbgrader.plugins.zipcollect import FileNameCollectorPlugin

class CSE6643Collector(FileNameCollectorPlugin):

    def __init__(self, file_id, **kwargs):
        FileNameCollectorPlugin.__init__(self, **kwargs)
        self._file_id = file_id

    def collect(self, submitted_file):
        groupdict = super().collect(submitted_file)
        if groupdict:
            groupdict['file_id'] = self._file_id + '.ipynb'
        return groupdict


class CSE6643CollectorHW2(CSE6643Collector):

    def __init__(self, **kwargs):
        CSE6643Collector.__init__(self, 'hw2', **kwargs)

class CSE6643CollectorHW3(CSE6643Collector):

    def __init__(self, **kwargs):
        CSE6643Collector.__init__(self, 'hw3', **kwargs)

class CSE6643CollectorHW4(CSE6643Collector):

    def __init__(self, **kwargs):
        CSE6643Collector.__init__(self, 'hw4', **kwargs)

class CSE6643CollectorHW5(CSE6643Collector):

    def __init__(self, **kwargs):
        CSE6643Collector.__init__(self, 'hw5', **kwargs)
