
import glob
import pathlib

files = glob.glob('downloaded/hw1/extracted/submissions/*')

name_to_sis = {}

for f in files:
    fname = pathlib.Path(f).name
    parts = fname.split('_')
    student = parts[0]
    if parts[1] == 'LATE':
        name_to_sis[student] = parts[2]
    else:
        name_to_sis[student] = parts[1]

print(name_to_sis)

