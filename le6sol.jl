using LinearAlgebra

function mgs(A)
  # Q,R = mgs(A), modified Gram-Schmidt orthogonalization
  n = size(A)[2]
  Q = copy(A)
  R = zeros(n,n)
  for i in 1:n
    R[i,i] = norm(Q[:,i])
    Q[:,i] ./= R[i,i]
    for j in i+1:n
      R[i,j] = Q[:,i]'*Q[:,j]
      Q[:,j] .-= R[i,j] .* Q[:,i]
    end
  end
  return (Q,R)
end

m = 100
n = 63

A = randn(m,n)
Q,R = mgs(A)
orthog_err = norm(I - Q'*Q,2)
recon_err = norm(A - Q*R,2)
triangle_err = norm(R - UpperTriangular(R),2)
println("Modified Gram-Schmidt:")
println("  orthogonality error: $orthog_err")
println("  reconstruction error: $recon_err")
println("  triangularity error: $triangle_err")
