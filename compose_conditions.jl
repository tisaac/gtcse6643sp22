using LinearAlgebra

m = 10; κ = 1.e10;

# Create an ill-conditioned matrix A
σ = LinRange(1, 1 / κ, m)
U = qr(randn(m,m)).Q
V = qr(randn(m,m)).Q
A = U * diagm(σ) * V'
println("κ(A): $(cond(A))")

# Create another matrix B such that AB is well-conditioned
B = V * diagm(1 ./ σ) * U'
println("κ(B): $(cond(B))")

println("κ(B*A): $(cond(B*A))")

# The solution we will uses for all four versionis
x = randn(m)

# Create a right-hand side in steps, then solve in steps
y = A * x
z_step = B * y
u = B \ z_step
x_step_step = A \ u

# Now solve with the product
x_step_prod = (B*A) \ z_step

# Create a right-hand side with the product, then solve in steps
z_prod = (B*A) * x

# Solve in steps
u = B \ z_prod
x_prod_step = A \ u

# Solve with the product
x_prod_prod = (B*A) \ z_prod

# Summarize errors
println("machine precision: $(eps())")

z_rel_err = norm(z_prod - z_step) / norm(z_prod)
println("||z_prod - z_step|| / ||z_prod|| = $z_rel_err")

rel_err_step_step = norm(x_step_step - x) / norm(x)
rel_err_step_prod = norm(x_step_prod - x) / norm(x)
rel_err_prod_step = norm(x_prod_step - x) / norm(x)
rel_err_prod_prod = norm(x_prod_prod - x) / norm(x)

println("Solution relative errors:")
println("  stepwise rhs, stepwise solution: $(rel_err_step_step), digits lost = $(log10(rel_err_step_step) - log10(eps()))")
println("  stepwise rhs, producte solution: $(rel_err_step_prod), digits lost = $(log10(rel_err_step_prod) - log10(eps()))")
println("  product  rhs, stepwise solution: $(rel_err_prod_step), digits lost = $(log10(rel_err_prod_step) - log10(eps()))")
println("  product  rhs, product  solution: $(rel_err_prod_prod), digits lost = $(log10(rel_err_prod_prod) - log10(eps()))")


