# CSE/MATH 6643: Numerical Linear Algebra | Spring, 2022

## Contact


|                | *name*             | *email*                                             | *office*                                            | *office hours*              |
| ---            | :----              | :----                                               | :----                                               | :----                       |
| **Instructor** | Toby Isaac         | [tisaac@cc.gatech.edu](mailto:tisaac@cc.gatech.edu) | [CODA S1323](https://goo.gl/maps/TsqtUC4uq4QicmpDA) | Tue 9:30-10:39 KACB 3121; [Fri 2-3 (virtual)][ohfri]  |
| **TA**         | Deeraj Pailla      |                                                     |                                                     |                             |
| **TA**         | Shiqin Zeng        |                                                     |                                                     |                             |
| **TA**         | Brandon Whitchurch |                                                     |                                                     |                             |

[ohfri]: https://bluejeans.com/717870493/8113

## Lectures

**In person:** Tuesdays & Thursdays, 8:00--9:15 a.m., [KACB 1443](https://goo.gl/maps/vgirLTxFPDRdrekY6)

**Remote:** <https://bluejeans.com/920279435/7463> (live, with recording available afterwards)

## Discussion

Piazza: <https://piazza.com/class/ky86bfaqahh51m>

## Description

Introduction to numerical solutions of the classical problems of linear algebra
including linear systems, least squares, and eigenvalue problems.

## Prerequisites

A four-credit or two-semester course in linear algebra, equivalent to MATH 1554
(subspaces, rank, Gram-Schmidt, QR, least squares, eigenvalues, eigenvectors,
singular value decomposition, vector norms). The assignments will require Julia
programming. An undergraduate level course in numerical methods (such as
MATH 4640) is strongly recommended.

## Topics

- Singular value decomposition
- Least squares problems and QR factorization
- Conditioning and stability
- Direct methods for solving linear systems
- Eigenvalue problems and the QR algorithm
- Introduction to iterative methods

## Learning Objectives

Students will develop facility with the methods of numerical linear algebra:
various factorizations, iterative methods, and their analysis. This leads to
the following larger learning objectives for students in this course. The
students will be able to:

- Model a real-world problem as a problem in numerical linear algebra
- Select or design a method or approach for solving a problem in numerical
  linear algebra
- Evaluate a method for its accuracy, stability, and computational cost
- Discuss efficiency implications in a computer implementation of a method,
  including parallel computing aspects
- Use Julia and other numerical software appropriately: understand when
  to use certain methods and their limitations

## Textbook

Trefethen & Bau, [Numerical Linear
Algebra](http://bookstore.siam.org/ot50/) (also a [Google Play ebook](https://play.google.com/store/books/details/Lloyd_N_Trefethen_Numerical_Linear_Algebra?id=JaPtxOytY7kC)).
(Become a SIAM member for a discount: student memberships are free)

## Additional resources

Golub & Van Loan, [Matrix Computations](https://www.amazon.com/Computations-Hopkins-Studies-Mathematical-Sciences/dp/1421407949/ref=sr_1_1?keywords=golub+van+loan+matrix+computations&qid=1641834853&sprefix=golub+van%2Caps%2C117&sr=8-1).

## Assignments & Grading

|                |     |
| ---            | --- |
| Participation  | 30% |
| Homeworks (6)  | 60% |
| Final Exam     | 10% |

* **Participation**: reading assignments, exercises where the solution is
  _worked out in the lecture_, and some discretionary points
  for participation in piazza threads (answering other students questions!),
  office hours, asking questions in class.

* **Homeworks**: one for each section in the textbook.  You can discuss
  homework problems with other students and on piazza, but the work you submit
  must be your own.  Directly copied solutions will be considered an honor code
  violation.  Graded with feedback within a week.  One lowest score will be
  dropped.

* **Final exam**: Take home, cumulative, closed book, to be
  turned in by **Thursday, May 5, 10:50 AM**.  Do not discuss with
  other students or seek outside resources.

**Grades** will be earned according to the standard scale:

|     |          |
| --- | ---      |
| A   | 90--100% |
| B   | 80--90%  |
| C   | 70--80%  |
| D   | 60--70%  |
| F   | 0--60%   |

* **Late policy**: 0.2% penalty for each hour late.

* **Passing** grades for Pass/Fail students are D or better.

## Collaboration policy

For all assignments, students may collaborate through discussion, but all
coding and writing should be done themselves.  Students who submit unattributed
material will be found in violation of the Honor code (see Academic Integrity
below).

## Academic Integrity

Georgia Tech aims to cultivate a community based on trust, academic integrity,
and honor. Students are expected to act according to the highest ethical
standards.  For information on Georgia Tech's Academic Honor Code, please visit
[http://www.catalog.gatech.edu/policies/honor-code/](http://www.catalog.gatech.edu/policies/honor-code/)
or [http://www.catalog.gatech.edu/rules/18/](http://www.catalog.gatech.edu/rules/18/).

## Accomodations for Individuals with Disabilities

If you are a student with learning needs that require special accommodation,
contact the Office of Disability Services at [(404)894-2563](tel:+14048942563)
or
[http://disabilityservices.gatech.edu/](http://disabilityservices.gatech.edu/),
as soon as possible, to make an appointment to discuss your special needs and
to obtain an accommodations letter.  Please also e-mail me as soon as possible
in order to set up a time to discuss your learning needs.


