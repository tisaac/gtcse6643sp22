# Homework 1: Fundamentals

1. (T&B 1.2) Suppose masses $m_1, m_2, m_3, m_4$ are located at positions
   $x_1, x_2, x_3, x_4$ in a line and connected by springs with spring constants
   $k_{12}, k_{23}, k_{34}$ whose natural lengths of extension are
   $l_{12}, l_{23}, l_{34}$. Let $f_1, f_2, f_3, f_4$ denote the rightward
   forces on the masses, e.g., $f_1 = k_{12}(x_2 - x_1 - l_{12})$.

   (a) Write the 4 $\times$ 4 matrix equation relating the column vectors $f$
   and $x$.  Let $K$ denote the matrix in this equation.

   (b) What are the dimensions of the entries of $K$ in the physics sense (e.g.,
   mass times time, distance divided by mass, etc.)?

   (c) What are the dimensions of $\det(K)$, again in the physics sense?

   (d) Suppose $K$ is given numerical values based on the units meters,
       kilograms, and seconds.  Now the system is rewritten with a matrix $K'$
       based on centimeters, grams, and seconds.  What is the relationship of
       $K'$ to $K$?  What is the relationship of $\det(K')$ to $\det(K)$.

2. (T&B 1.3) We say that a square or rectangular matrix $R$ with entries
   $r_{ij}$ is _upper-triangular_ if $r_{ij} = 0$ for $i > j$.
   By considering what space is spanned by the first $n$ columns of $R$,
   show that if $R$ is a nonsingular $m \times m$ upper-triangular matrix,
   then $R^{-1}$ is also upper-triangular.
   (The analogous result also holds for lower-triangular matrices.)

3. (T&B 2.5) Let $S \in \mathbb{C}^{m \times m}$ be _skew-hermitian_, i.e.,
   $S^* = -S$.

   (a) Show that the eigenvalues of $S$ are pure imaginary.

   (b) Show that $I-S$ is nonsingular.

   (c) Show that the matrix $Q = (I-S)^{-1}(I + S)$, known as the _Cayley
       transform_ of $S$, is unitary. (This is a matrix analogue of a linear
       fractional transformation $(1+s)/(1-s)$, which maps the left half of the
       complex $s$-plane conformally onto the unit disk.)

4. (T&B 2.6) If $u$ and $v$ are $m$-vectors, the matrix $A = I + u v^*$ is
   known as a _rank-one perturbation of the identity_.  Show that if $A$ is
   nonsingular, then its inverse has the form $A^{-1} = I + \alpha u v^*$ for
   some scalar $\alpha$, and give an expression for $\alpha$.  For what $u$ and
   $v$ is $A$ singular?  If it is singular, what is
   $\mathop{\mathrm{null}}(A)$?

   \pagebreak

5. (T&B 3.3) Vector and matrix $p$-norms are related by various inequalities,
   often involving the dimensions $m$ or $n$.  For each of the following,
   verify the inequality and give an example of a nonzero vector or matrix (for
   general $m$,$n$) for which equality is achieved. In this problem $x$ is an
   $m$-vector and $A$ is an $m \times n$ matrix.

   (a) $\| x \|_\infty \leq \| x \|_2$.

   (b) $\| x \|_2 \leq \sqrt{m} \| x \|_\infty$.

   (c) $\| A \|_\infty \leq \sqrt{n} \| A \|_2$.

   (d) $\| A \|_2 \leq \sqrt{m} \| A \|_\infty$.

6. (T&B 3.5) It is true that if $E = u v^*$, then $\|E\|_2 = \|u\|_2 \|v\|_2$.
   Is the same true for the Frobenius norm, i.e., $\|E\|_F = \|u\|_F \|v\|_F$?
   Prove it or give a counterexample.

7. (T&B 3.6) Let $\| \cdot \|$ denote any norm on $\mathbb{C}^{m}$.  The
   corresponding _dual norm_ $\| \cdot \|'$ is defined by the formula
   $\|x\|' = \sup_{\|y\|=1} |y^* x|.$

   (a) Prove that $\| \cdot \|'$ is a norm.

   (b) Let $x,y\in \mathbb{C}^m$ with $\|x\| = \|y\| = 1$ be given.  Show that
       there exists a rank-one matrix $B = yz^*$ such that $Bx = y$ and
       $\|B\|=1$, where $\|B\|$ is the matrix norm of $B$ induced by the vector
       norm $\|\cdot\|$. You may use the following lemma, without proof: given
       $x\in \mathbb{C}^m$, there exists a nonzero $z\in \mathbb{C}^m$ such
       that $|z^* x| = \|z\|' \|x\|$.

8. (T&B 4.5) Theorem T&B 4.1 asserts that every $A\in \mathbb{C}^{m \times n}$
   has an SVD $A = U \Sigma V^*$.  Show that if $A$ is real, then it has a real
   SVD ($U\in\mathbb{R}^{m \times m}, V\in\mathbb{R}^{n \times n}$).

   **Update:** You may use the following two facts without proof.

   1. Every real symmetric matrix $A^*A \in \mathbb{R}^{n \times n}$ has
   a real eigendecomposition $A^*A = Q \Lambda Q^*$, where
   $Q\in \mathbb{R}^{n \times n}$ is orthogonal (real and unitary),
   and $\Lambda = \mathrm{diag}(\lambda_1,\dots,\lambda_n)$
   is a real diagonal matrix such that $\lambda_1 \geq \lambda_2 \geq \dots
   \lambda_n \geq 0.$

   2. Given a set of real, orthonormal vectors $\{u_1,\dots, u_r\}\subset
   \mathbb{R}^{m}$ with $r < m$, it is possible to find additional vectors
   $\{u_{r+1},\dots,u_m\} \subset \mathrm{R}^m$
   such that $U = [u_1 | \dots | u_r | u_{r+1} | \dots | u_m]$ is an orthogonal
   matrix.


