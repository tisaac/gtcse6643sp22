# Homework 1: Fundamentals (**Solutions**)

1. (T&B 1.2) Suppose masses $m_1, m_2, m_3, m_4$ are located at positions
   $x_1, x_2, x_3, x_4$ in a line and connected by springs with spring
   constants $k_{12}, k_{23}, k_{34}$ whose natural lengths of extension are
   $l_{12}, l_{23}, l_{34}$. Let $f_1, f_2, f_3, f_4$ denote the rightward
   forces on the masses, e.g., $f_1 = k_{12}(x_2 - x_1 - l_{12})$.

   (a) **5%** Write the 4 $\times$ 4 matrix equation relating the column vectors
       $f$ and $x$.  Let $K$ denote the matrix in this equation.

       ---

       First just write all three equations:

       $$
       \begin{aligned}
       f_1 &=   k_{12} (x_2 - x_1 - l_{12}) \\
       f_2 &= - k_{12} (x_2 - x_1 - l_{12}) + k_{23} (x_3 - x_2 - l_{23}) \\
       f_3 &= - k_{23} (x_3 - x_2 - l_{23}) + k_{34} (x_4 - x_3 - l_{34}) \\
       f_4 &= - k_{34} (x_4 - x_3 - l_{34}).
       \end{aligned}
       $$

       Then you separate the $x$ values from the $l$ values:

       $$
       \begin{aligned}
       f_1 &=   k_{12} (x_2 - x_1) &- k_{12} l_{12} \\
       f_2 &= - k_{12} (x_2 - x_1) + k_{23} (x_3 - x_2) &+ (k_{12} l_{12} - k_{23} l_{23}) \\
       f_3 &= - k_{23} (x_3 - x_2) + k_{34} (x_4 - x_3) &+ (k_{23} l_{23} - k_{34} l_{34}) \\
       f_4 &= - k_{34} (x_4 - x_3) &+ k_{34} l_{34}.
       \end{aligned}
       $$

       Now we can turn the action of the $k$ values on the $x$ values into a
       matrix, and we can turn other values in to vectors:

       $$
       \begin{bmatrix}
       f_1 \\ f_2 \\ f_3 \\ f_4
       \end{bmatrix}
       =
       \begin{bmatrix}
       - k_{12} &  k_{12}         &                 & \\
         k_{12} & -k_{12} -k_{23} &  k_{23}         & \\
                &          k_{23} & -k_{23} -k_{34} &  k_{34} \\
                &                 &          k_{34} & -k_{34} \\
       \end{bmatrix}
       \begin{bmatrix}
       x_1 \\ x_2 \\ x_3 \\ x_4
       \end{bmatrix}
       +
       \begin{bmatrix}
        -k_{12} l_{12} \\
       ( k_{12} l_{12} - k_{23} l_{23}) \\ 
       ( k_{23} l_{23} - k_{34} l_{34}) \\ 
         k_{34} l_{34}
       \end{bmatrix}.
       $$

       ---

   (b) **5%** What are the dimensions of the entries of $K$ in the physics sense
       (e.g., mass times time, distance divided by mass, etc.)?

       ---

       Each $x_i$ has units of distance, and each $f_i$ has units of force,
       which is mass $\cdot$ distance $\cdot$ time${}^{-2}$.  The product of
       the matrix and $x$ has to have the same units as $f$, so the matrix
       must have units of mass $\cdot$ time${}^{-2}$.

       ---

   (c) **5%** What are the dimensions of $\det(K)$, again in the physics sense?

       ---

       The determinant of an $m \times m$ matrix is a [homogeneous
       polynomial](https://en.wikipedia.org/wiki/Homogeneous_polynomial) of
       degree $m$ of its entries.  In this case $m = 4$, so the units of
       $\det(K)$ are (mass $\cdot$ time${}^{-2}$)${}^4$.

       ---

   (d) **5%** Suppose $K$ is given numerical values based on the units meters,
       kilograms, and seconds.  Now the system is rewritten with a matrix
       $K'$ based on centimeters, grams, and seconds.  What is the
       relationship of $K'$ to $K$?  What is the relationship of $\det(K')$
       to $\det(K)$.

       ---

       If an entry of $K$ had a value of $\alpha$ kilograms $\cdot$
       seconds${}^{-2}$, then because 1 kilogram = 1000 grams, the entry in
       $K'$ would be $1000\alpha$ (grams $\cdot$ seconds${}^{-2}$), that is,
       $K' = 1000 K$.  From the previous answer, we conclude that
       $\det(K') = 1000^4 \det(K) = 10^{12} \det(K)$.

       All of this is to show how sensitive the determinant of a matrix is:
       this was only a $4 \times 4$ system.  In practice we work with
       matrices with dimensions in the thousands, millions, and billions. We
       have no hope of relying on the determinant as a quantify to determine
       the singularity of a matrix in these cases.

       ---

2. (T&B 1.3) **10%** We say that a square or rectangular matrix $R$ with entries
   $r_{ij}$ is _upper-triangular_ if $r_{ij} = 0$ for $i > j$.
   By considering what space is spanned by the first $n$ columns of $R$,
   show that if $R$ is a nonsingular $m \times m$ upper-triangular matrix,
   then $R^{-1}$ is also upper-triangular.
   (The analogous result also holds for lower-triangular matrices.)

   ---

   Let $Z = [z_1 | \dots | z_m]$ be the inverse of $R$.  Looking at just
   the $j$th column of the identity $I = ZR$, we see
   $$
   e_j = \sum_{i=1}^m z_i r_{ij} = \sum_{i=1}^j z_i r_{ij} =
   r_{jj} z_j + \sum_{i=1}^{j-1} z_i r_{ij}.
   $$
   Rearranging, we have
   $$
   -r_{jj} z_j = -e_j + \sum_{i=1}^{j-1} z_i r_{ij}.
   $$
   If $j=1$, this implies $-r_{11} z_1 = -e_1$, and so $z_1 \in 
   \mathrm{span}\{e_1\}.$
   Now suppose we have proved that $z_j \in \mathrm{span}\{e_1, \dots,
   e_j\}$ for every $j < n$.  Then
   $$
   -r_{nn} z_n = -e_n + \underbrace{\sum_{i=1}^{n-1} z_i r_{in}}_{\in
   \mathrm{span} \{e_1, \dots, e_{n-1}\}}.
   $$
   Now $r_{nn}$ cannot be zero, because then the left hand side is the zero vector, and that would imply $e_n = \sum_{i=1}^{n-1} z_i r_{in} \in
   \mathrm{span}\{e_1, \dots, e_{n-1}\}$, which is impossible. So
   $$
   z_n = \frac{1}{r_{nn}} e_n - \sum_{i=1}^{n-1} z_i \frac{r_{in}}{r_{nn}}
   \in \mathrm{span}\{e_1,\dots, e_n\}.
   $$
   By induction, we conclude that $z_j \in \mathrm{span}\{e_1, \dots, e_j\}$ for every $j\in \{1, \dots, m\}$, which is another way of saying $Z$ is upper-triangular.

   ---

3. (T&B 2.5) Let $S \in \mathbb{C}^{m \times m}$ be _skew-hermitian_, i.e.,
   $S^* = -S$.

   (a) **5%** Show that the eigenvalues of $S$ are pure imaginary.

       ---

       Let $v$ be a nonzero eigenvector with eigenvalue $\lambda$:

       $$
       \begin{aligned}
       \lambda &= \lambda \frac{v^* v}{v^*v} =
       \frac{v^*(\lambda v)}{v^* v}
       =
       \frac{v^*(S v)}{v^* v}
       =
       \frac{(S^*v)^*v}{v^* v}
       \\
       &=
       -\frac{(Sv)^*v}{v^* v}
       =
       -\frac{(\lambda v)^*v}{v^* v}
       =
       -\bar{\lambda} \frac{v^*v}{v^*v}
       =
       -\bar{\lambda}.
       \end{aligned}
       $$
       This says that
       $$
       \begin{aligned}
       &\phantom{=}\ \mathrm{Re}(\lambda) + \mathrm{i}\ \mathrm{Im}(\lambda)
       = -\overline{\mathrm{Re}(\lambda) + \mathrm{i}\ \mathrm{Im}(\lambda)}
       \\
       &= -(\mathrm{Re}(\lambda) - \mathrm{i}\ \mathrm{Im}(\lambda))
       = -\mathrm{Re}(\lambda) + \mathrm{i}\ \mathrm{Im}(\lambda).
       \end{aligned}
       $$
       Therefore $\mathrm{Re}(\lambda) = - \mathrm{Re}(\lambda)$.  We
       conclude $\mathrm{Re}(\lambda) = 0.$

       ---

   (b) **5%** Show that $I-S$ is nonsingular.

       ---

       Let $\lambda$ be an eigenvalue of $I-S$ with nonzero eigenvector $v$.
       Then $S v = -(I - S) v + v = -\lambda v + v = (1 - \lambda v),$ that
       is, $(1 - \lambda)$ is an eigenvalue of $S$.  From the previous
       question, $\mathrm{Re}(1 - \lambda) = 0$, that is
       $\mathrm{Re}(\lambda) = 1$.  Therefore $\lambda \neq 0$.  Because
       none of the eigenvalues of $(I - S)$ is zero, it is nonsingular.

       ---

   (c) **5%** Show that the matrix $Q = (I-S)^{-1}(I + S)$, known as the _Cayley
       transform_ of $S$, is unitary. (This is a matrix analogue of a linear
       fractional transformation $(1+s)/(1-s)$, which maps the left half of the
       complex $s$-plane conformally onto the unit disk.)

       ---

       To show a matrix is unitary we must show $Q^* Q = Q Q^* = I$.
       Because $S$ is skew-hermitian, we know $(I + S)^* = (I - S)$
       and $(I - S)^* = (I + S)$.  We can also show for any matrix that
       $(I - S)(I + S) = I - S^2 = (I + S)(I-S)$, that is $(I + S)$ and $(I-S)$ commute.
       So
       $$
       \begin{aligned}
       Q^* Q
       &= 
       ((I - S)^{-1}(I + S))^* (I - S)^{-1}(I + S)
       &\text{(expand definition)}
       \\
       &=
       (I + S)^*(I - S)^{-*} (I - S)^{-1}(I + S)
       &\text{(expand adjoints)}
       \\
       &=
       (I + S)^*((I-S)(I-S)^*)^{-1}(I + S)
       &\text{(contract inverse)}
       \\
       &=
       (I - S)((I-S)(I+S))^{-1}(I + S)
       &\text{(skew-hermitian)}
       \\
       &=
       (I - S)((I+S)(I-S))^{-1}(I + S)
       &\text{(commute terms)}
       \\
       &=
       (I - S)(I-S)^{-1}(I+S)^{-1}(I + S)
       &\text{(expand inverse)}
       \\
       &= I.
       \end{aligned}
       $$

       ---

4. (T&B 2.6) **10%** If $u$ and $v$ are $m$-vectors, the matrix $A = I + u v^*$ is
   known as a _rank-one perturbation of the identity_.  Show that if $A$ is
   nonsingular, then its inverse has the form $A^{-1} = I + \alpha u v^*$ for
   some scalar $\alpha$, and give an expression for $\alpha$.  For what $u$ and
   $v$ is $A$ singular?  If it is singular, what is
   $\mathop{\mathrm{null}}(A)$?

   ---

   Multiplying $(I + uv^*)$ and $(I + \alpha u v^*)$, we see
   by expanding the terms that
   $$
   (I + uv^*)(I + \alpha u v^*) = I  + uv^* + \alpha u v^* + \alpha (v^* u) uv^* = I + (1 + \alpha(1 + v^*u)) uv^*.
   $$
   If $1 + \alpha(1 + v^*u) = 0$, then this means
   $(I + uv^*)(I + \alpha u v^*) = I.$
   Rearranging for $\alpha$, we get
   $$
   \alpha = \frac{-1}{1 + v^* u}.
   $$
   This number exists if and only if $1 + v^* u \neq 0$: in other words, $I
   + uv^*$ is singular if $v^*u = -1$. If $w \in \mathrm{null}(A)$, then $(I
   + uv^*)w = 0$.  Rearranging, we get $w = -u(v^*w)$.  In other words,
   $\mathrm{null}(A) \subseteq \mathrm{span}\{u\}$, with equality if and
   only if $v^*u = -1$.

   ---

5. (T&B 3.3) Vector and matrix $p$-norms are related by various inequalities,
   often involving the dimensions $m$ or $n$.  For each of the following,
   verify the inequality and give an example of a nonzero vector or matrix (for
   general $m$,$n$) for which equality is achieved. In this problem $x$ is an
   $m$-vector and $A$ is an $m \times n$ matrix.

   (a) **5%** $\| x \|_\infty \leq \| x \|_2$.

       ---
    
       Let $j$ be an index such that $|x_j| = \max_{1 \leq i \leq m} |x_i|$.
       Then
       $$
       \| x \|_\infty^2 
       =
       (\max_{1\leq i \leq m} |x_i|)^2
       =
       |x_j|^2
       \leq
       |x_j|^2 + \sum_{\stackrel{i=1}{i\neq j}}^m |x_i|^2
       =
       \sum_{i=1}^m |x_i|^2
       =
       \| x \|_2^2.
       $$

       An example where the bound is tight is $x = e_i$ for any $i$.

       ---

   (b) **5%** $\| x \|_2 \leq \sqrt{m} \| x \|_\infty$.

       ---
    
       $$
       \| x \|_2^2
       =
       \sum_{i=1}^m |x_i|^2
       \leq
       \sum_{i=1}^m \max_{1\leq j \leq m} |x_j|^2
       =
       (\max_{1\leq j \leq m} |x_j|^2) \sum_{i=1}^m 1
       =
       m \| x \|_\infty^2.
       $$

       An example where the bound is tight is $\mathbf{1}$, a vector of all
       ones, because its maximum value is $1$ but the sum of the squares of
       the entries is $m$.
    
       ---

   (c) **5%** $\| A \|_\infty \leq \sqrt{n} \| A \|_2$.

       ---
       
       $$
       \begin{aligned}
       \| A \|_\infty
       &=
       \max_{x \neq 0} \frac{\|A x\|_\infty}{\|x\|_\infty}
       \\
       &\leq
       \max_{x \neq 0} \frac{\|A x\|_2}{\|x\|_\infty}
       &\text{(by (a))}
       \\
       &\leq
       \max_{x \neq 0} \frac{\sqrt{n}\|A x\|_2}{\|x\|_2}
       &\text{(by (b))}
       \\
       &=
       \sqrt{n} \| A \|_2.
       \end{aligned}
       $$

       An example where the bound is tight is an outer product of the
       examples from (a) and (b), that is, choose $A = e_i \mathbf{1}^*$.
       It is worked out in the book that $\|A\|_\infty$ is the maximum row
       sum, which is $n$, and in Example 3.6 that $\|A\|_2$ for an
       outer product is the product of the vector 2-norms, which is
       $1 \cdot \sqrt{n}$, so $\|A\|_\infty = \sqrt{n}\|A\|_2.$
    
       ---

   (d) **5%** $\| A \|_2 \leq \sqrt{m} \| A \|_\infty$.

       ---
    
       $$
       \begin{aligned}
       \| A \|_2
       &=
       \max_{x \neq 0} \frac{\|A x\|_2}{\|x\|_2}
       \\
       &\leq
       \max_{x \neq 0} \frac{\sqrt{m}\|A x\|_\infty}{\|x\|_2}
       &\text{(by (b))}
       \\
       &\leq
       \max_{x \neq 0} \frac{\sqrt{m}\|A x\|_\infty}{\|x\|_\infty}
       &\text{(by (a))}
       \\
       &=
       \sqrt{m} \| A \|_\infty.
       \end{aligned}
       $$

       An example where the bound is tight is the adjoint of the
       previous example, choose $A = \mathbf{1} e_i^*$.
       The maximum row sum is 1, and the product of the vector norms is
       $\sqrt{m} \cdot 1$, so $\|A\|_2 = \sqrt{m} \|A\|_\infty.$

       ---

6. (T&B 3.5) **5%** It is true that if $E = u v^*$, then $\|E\|_2 = \|u\|_2 \|v\|_2$.
   Is the same true for the Frobenius norm, i.e., $\|E\|_F = \|u\|_F \|v\|_F$?
   Prove it or give a counterexample.

   ---

   $$\|E\|_F^2 = \mathrm{tr}(E^*E) = \mathrm{tr}(v u^* u v^*) = \|u\|_2^2 \mathrm{tr}(v v^*)
   = \|u\|_2^2 \mathrm{tr}(v^* v) = \|u\|_2^2 \|v\|_2^2.$$

   Because for vectors the Frobenius norm is the 2-norm, this proves $\|E\|_F = \|u\|_F \|v\|_F.$

   ---

7. (T&B 3.6) Let $\| \cdot \|$ denote any norm on $\mathbb{C}^{m}$.  The corresponding
   _dual norm_ $\| \cdot \|'$ is defined by the formula $\|x\|' = \sup_{\|y\|=1} |y^* x|.$

   (a) **5%** Prove that $\| \cdot \|'$ is a norm.

       ---

       - $|y^*x| \geq 0$ for all $y$ and $x$, so $\sup_{\|y\| = 1} |y^*x|
         \geq 0$ for all $x$, so $\|x\|' \geq 0$ for all $x$.

         Now suppose $x \neq 0$.  Therefore
         $$
         \|x\|' = \sup_{\|y\|=1} |y^*x| \geq \left|\frac{x^*}{\|x\|} x\right| = \frac{\|x\|^2}{\|x\|} > 0.
         $$
         And of course if $x = 0$, then
         $$
         \|x\|' = \sup_{\|y\|=1} |y^*x| = \sup_{\|y\| = 1} 0 = 0. $$
         Therefore $\|x\|' = 0$ if and  only if $x = 0$.

       - Let $x$ and $z$ be given:

         $$
         \begin{aligned}
         \|x + z\|' &= 
         \sup_{\|y\|=1} |y^*(x + z)|
         \\
         &=
         \sup_{\|y\|=1} |y^*x + y^*z|
         \\
         &\leq
         \sup_{\|y\|=1} |y^*x| + |y^*z|
         \\
         &\leq
         \sup_{\|y\|=1} |y^*x| + \sup_{\|y\| = 1} |y^*z|
         \\
         &= \|x\|' + \|z'\|.
         \end{aligned}
         $$

       - Let $x$ and $\alpha$ be given.

         $$
         \begin{aligned}
         \|\alpha x\|' &= 
         \sup_{\|y\|=1} |y^*(\alpha x)|
         \\
         &=
         \sup_{\|y\|=1} |\alpha| |y^*x|
         \\
         &=
         |\alpha| \sup_{\|y\|=1} |y^*x|
         \\
         &= |\alpha|\|x\|'.
         \end{aligned}
         $$

       ---

   (b) **5%** Let $x,y\in \mathbb{C}^m$ with $\|x\| = \|y\| = 1$ be given.  Show that
       there exists a rank-one matrix $B = yz^*$ such that $Bx = y$ and $\|B\|=1$,
       where $\|B\|$ is the matrix norm of $B$ induced by the vector norm $\|\cdot\|$.
       You may use the following lemma, without proof: given $x\in \mathbb{C}^m$, there exists
       a nonzero $z\in \mathbb{C}^m$ such that $|z^* x| = \|z\|' \|x\|$.

       ---

       Using the lemma, let $z$ be such that $|z^*x| = \|z\|' \|x\| = \|z\|'.$
       Then define
       $$B = \frac{\overline{z^*x}}{(\|z\|')^2} yz^*.$$
       First we show that $Bx = y:$
       $$Bx = \frac{\overline{z^*x}}{(\|z\|')^2} y(z^*x) = \frac{|z^*x|^2}{(\|z\|')^2} y = y.$$
       Now we show that $\|B\|=1$:
       $$
       \begin{aligned}
       \|B\| &= \sup_{\|v\|=1} \|Bv\|
       \\
       &= \sup_{\|v\|=1} \big\|\frac{\overline{z^*x}}{(\|z\|')^2} y(z^*v) \big\|
       \\
       &= \sup_{\|v\|=1} \frac{|\overline{z^*x}|}{(\|z\|')^2}
       \|y\||z^*v|
       \\
       &= \frac{|\overline{z^*x}|}{(\|z\|')^2} \|y\| \sup_{\|v\|=1} |z^*v|
       \\
       &= \frac{|\overline{z^*x}|}{(\|z\|')^2} \|y\| \|z\|'
       \\
       &= \frac{|\overline{z^*x}|}{\|z\|'} = 1.
       \end{aligned}
       $$

       ---

8. (T&B 4.5) **10%** Theorem T&B 4.1 asserts that every $A\in \mathbb{C}^{m \times n}$
   has an SVD $A = U \Sigma V^*$.  Show that if $A$ is real, then it has a real
   SVD ($U\in\mathbb{R}^{m \times m}, V\in\mathbb{R}^{n \times n}$).

   **Update:** You may use the following two facts without proof.

   1. Every real symmetric matrix $A^*A \in \mathbb{R}^{n \times n}$ has
   a real eigendecomposition $A^*A = Q \Lambda Q^*$, where
   $Q\in \mathbb{R}^{n \times n}$ is orthogonal (real and unitary),
   and $\Lambda = \mathrm{diag}(\lambda_1,\dots,\lambda_n)$
   is a real diagonal matrix such that $\lambda_1 \geq \lambda_2 \geq \dots
   \lambda_n \geq 0.$

   2. Given a set of real, orthonormal vectors $\{u_1,\dots, u_r\}\subset
   \mathbb{R}^{m}$ with $r < m$, it is possible to find additional vectors
   $\{u_{r+1},\dots,u_m\} \subset \mathrm{R}^m$
   such that $U = [u_1 | \dots | u_r | u_{r+1} | \dots | u_m]$ is an orthogonal
   matrix.

   ---

   Let $A^*A = Q \Lambda Q^*$ as in the lemma.
   Let $q_i$ be one of the column vectors of $Q$.
   Let $w_i = A q_i$.  If $i \neq j$, then
   $$
   (w_i)^* (w_j) = (A q_i)^* (A q_j) = q_i^* A^* A q_j = q_i^* Q \Lambda Q^* q_j = e_i^* \Lambda e_j = \lambda_i \delta_{ij}.
   $$
   Therefore $w_i^*$ is orthogonal to $w_j^*$ if $i\neq j$. Now let $r \leq n$
   be such that $\lambda_r > 0$ and $\lambda_{r+1} = \lambda_{r+2} = \dots
   \lambda_n = 0.$ If $i \leq r$, then $(w_i)^* (w_i) = \lambda_i > 0,$ 
   and we can define $u_i = \frac{w_i}{\sqrt{\lambda_i}}$.
   Using the second lemma, we can define vectors $\{u_{r+1},\dots,u_m\}$
   such that $U = [u_1 | \dots | u_r | u_{r+1} | \dots | u_m ]$ is orthogonal.
   Letting $\sigma_i = \sqrt{\lambda_i}$ and $\Sigma
   = \mathrm{diag}(\sigma_1,\dots,\sigma_n)$, the claim is that
   $A = U \Sigma Q^*$ is a real SVD of $A$.
   Now $U$ and $Q$ are orthogonal matrices and $\Sigma$ is real, diagonal,
   and nonnegative, so it is clear that $U \Sigma Q^*$ is the SVD of _some_ matrix:
   we just have to show it is the SVD of $A$.  Applying this SVD to $q_i$ we get
   $$
   U \Sigma Q^* q_i = U \Sigma e_i = U e_i \sqrt{\lambda_i} = u_i \sqrt{\lambda_i} = \begin{cases}
   \frac{Aq_i}{\sqrt{\lambda_i}}\sqrt{\lambda_i} = Aq_i, & i \leq r \\
   u_i \cdot 0 = 0 = A q_i, & i > r.
   \end{cases}
   $$
   Therefore $U \Sigma Q^* q_i = A q_i$ for each $i$, which shows that $U \Sigma Q^* = A$.

   ---
