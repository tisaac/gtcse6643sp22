---
title: 'Final exam | CSE/MATH 6643 | Spring 2022'
geometry:
  - margin=1in
---

# Instructions

This exam has six questions, one for each section of the book.  Each question
is weighted equally, and only the highest four scores count.

In general you should show your work to have the chance to get partial credit,
but you may use results from the book without proof, as long as it is clear
which results you are using at each step.

You will submit your answers as a single pdf on canvas.  You can type your
answers or work out your solutions by hand and submit a document containing
scans or high quality photos of your answers.

You **must** turn in your submission by **10:50 a.m., Thursday, May 5**.

This is a closed book, closed note exam.  You should only look at the questions
starting on the following page once you have finished studying.  Once you start
working, you may not consult the book, your notes, piazza, other students, or
any other outside resources.  Doing so will be considered an honor code
violation.  Clarification questions may be asked privately of instructors on
piazza.

Once you start working there is no time limit (other than the final submission
deadline, see above).  The exam was written so that it should be reasonable to
complete it in three hours, but you do not have to complete it in one sitting.

Best of luck to you!


\pagebreak

$${}$$
$${}$$
$${}$$
$${}$$
$${}$$
$${}$$
$${}$$
$${}$$
$${}$$
$${}$$
$${}$$
$${}$$
$${}$$
$${}$$
$${}$$

**(Turn the page to start the exam)**

\pagebreak

# Q1

Define $f: \mathbb{C}^{m \times n} \to \mathbb{R}$ by
$$f(A) = \sup_{B \neq 0 \in \mathbb{C}^{m \times n}} \frac{|\mathrm{tr}(B^*A)|}{\|B\|_2},$$
where $\|\cdot \|_2$ is the induced matrix 2-norm.

## Q1.a (40%)

Prove that $f$ defines a norm on $\mathbb{C}^{m \times n}$.

## Q1.b (40%)

Prove that $f$ is unitarily invariant: that $f(A) = f(QA)$ for every unitary
$Q$ and $f(A) = f(A\Phi)$ for every unitary $\Phi$.  (A fact that you may use
without proof is that the trace of a product of matrices is invariant under
cyclic permutations: $\mathrm{tr}(AB) = \mathrm{tr}(BA)$, $\mathrm{tr}(ABC)
= \mathrm{tr}(BCA)$, etc.)

## Q1.c (20%)

Use part (b) to show that $f$ is an explicit function of the singular values of
$A$ and show what that function is.

\pagebreak

# Q2

Write an algorithm for computing the QR factorization of an upper Hessenberg
matrix $H \in \mathbb{C}^{m \times m}$.  Your algorithm can represent $Q$ in
any form, and should construct $R$ from $H$ in place.

Your algorithm should:

- be correct; (20%)
- be backward stable; (30%)
- have optimal complexity (up to a factor of a leading coefficient). (30%)

You should correctly compute the leading term in the work required by your
algorithm. (20%)

\pagebreak

# Q3

We can modify the least squares problem by introducing a nonstandard inner
product.  Let $M \in \mathbb{C}^{m \times m}$ be a hermitian positive definite
matrix, let $A \in \mathbb{C}^{m \times n}$, ($m \geq n$) be a rank $n$ matrix,
and let $b \in \mathbb{C}^m$ be given.  We want to compute $x\in \mathbb{C}^n$
that minimizes the quantity
$$
\| b - A x \|_M,
$$
where $\| v \|_M = \sqrt{ v^* M v }$.

## Q3.a  (40%)

For the ordinary least squares problem, the residual $r = b - Ax$ is orthogonal
to $\mathrm{range}(A)$.  State and prove a similar orthogonality condition for
this modified least squares problem.

## Q3.b (40%)

The image of the solution in $\mathbb{C}^m$ is $y = Ax$. Give a formula for
a matrix $P$ such that $y = P b$.

## Q3.c (20%)

As with other norms, we can induce a matrix norm from the vector norm $\|\cdot
\|_M$.  Show that $\|P\|_M = 1$.

\pagebreak

# Q4

Let $A$ be a real symmetric positive definite matrix with the following
properties:

- (a) $a_{ij} \leq 0$ for every $(i,j)$ such that $i \neq j$;
- (b) $a_{ii} > \sum_{j\neq i} |a_{ji}|$ for every $i$.

Let $L L^*$ be the Cholesky factorization of $A$.  Prove that $L_{ij} \leq 0$
for every $(i,j)$ such $i \neq j$.

_Hint:_ if a real symmetric positive definite matrix has property (a), then
another way to state property (b) is that the vector $A \mathbf{1}$ is
positive, where $\mathbf{1}$ is the vector of ones.

\pagebreak

# Q5

Suppose $A$ is a normal $n \times n$ matrix with eigenvalues at the $n$ roots
of unity, $\lambda_k = e^{2\pi \frac{k}{n} \mathrm{i}}$ for $k = 1, \dots, n$.

## Q5.a (25%)

Does the power iteration with a random initial vector $v$ converge to an
eigenvector? If yes, what is the convergence rate?  If no, why not?

## Q5.b (75%)

Now consider the matrix $A + I$.  Does the power iteration with a random
initial vector converge to an eigenvector?  If yes, what is the convergence
rate?  If no, why not?

\pagebreak

# Q6

GMRES is used to solve $Ax = b$ using only matrix-vector products with $A$.  It
is usually assumed that the matrix $A$ is nonsingular.  But in this question we
try to use GMRES on singular matrices.  (In this question, we mean GMRES with
an initial guess of $x_0 = 0$.)

## Q6.a (40%)

Suppose $A$ is diagonalizable: prove that if $b \in \mathrm{range}(A)$, then
GMRES converges to an $x$ such that $Ax = b$.

## Q6.b (20%)

Give an example of a $2 \times 2$ singular matrix $A$ and $b \in
\mathrm{range}(A)$ such that GMRES does not converge to an $x$ such that $Ax
= b$.

## Q6.c (40%)

Suppose $A$ is a normal matrix and $b$ is not in $\mathrm{range}(A)$.  Does
GMRES converge?  If so, what $x$ does GMRES converge to?
