# Fundamentals

1.
  1. Introduction
  2. Matrix times a vector, example 1.1 Vandermonde matrix
  3. A matrix times a matrix, example 1.2, exercise 1.1
  4. Range and nullspace, rank, inverse, theorem 1.3
  5. A matrix inverse times a vector

2.
  1. Adjoint, inner product, exercise 2.3
  2. Orthogonal vector, theorem 2.1, components of a vector
  3. Unitary matrices, multiplication by a unitary matrix, exercise 2.1, 2.4
  4. Vector norms, exercise 3.1
  5. Matrix norms induced by vector norms, examples 3.1, 3.2, 3.3, 3.4, exercise 3.2

3.
  1. Cauchy-Schwarz and Hölder Inequalities, example 3.5, example 3.6, Bounding $\|AB\|$ in an Induced Matrix Norm
  2. General Matrix Norms, Invariance under unitary multiplication, theorem 3.1
  3. SVD: Geometric observation
  4. Reduced SVD, Full SVD, Formal Definition, exercise 4.4

4.
  1. SVD existence and uniqueness, Theorem 4.1
  2. A Change of Bases, SVD vs. Eigenvalue Decomposition
  3. Matrix Properties via the SVD, Theorems 5.1, 5.2, 5.3, 5.4, 5.5
  4. Low-Rank Approximations, Theorem 5.7, 5.8, 5.9, exercise 5.2, exercise 5.4

---

5.
  1. Projectors and Complementary Projectors
  2. Orthogonal Projectors, Exercise 6.1
  3. Projection with an Orthogonal Basis vs. Projection with an Arbitrary Basis
  4. Reduced QR Factorization & Full QR Factorization
  5. QR by Projections
  LE: 6.1:
    $$(I-2P)^*(I-2P) = I - 2P^* -2P + 4P^*P = I - 4 P + 4P^2 = I - 2$$

6.
  W: 6.4
  1. Classical Gram-Schmidt, QR Existence and Uniqueness
  (1.5 pre-record) Example: When Vectors Become Continuous Functions
  2. Solution of $Ax = b$ by QR factorization
  3. Modified Gram-Schmidt & Triangular Orthogonalization
  4. Operation Count & Counting Operations Geometrically
  LE: 8.2:
    
7. 
  W: 7.3
  1. Householder and Gram-Schmidt, Triangularizing by Introducing Zeros
  2. Householder Reflectors, Exercise 10.1
  3. The Algorithm
  4. Applying or Forming Q
  5. Operation Count
  LE: 10.1

8.
  1. Least Squares Problems, Example: Polynomial Data-Fitting
  2. Orthogonal Projection and the Normal Equations
  3. Pseudoinverse, Normal Equations, Exercise 11.1
  4. QR Factorization, SVD
  LE: 11.1

HW2:
  L6: 6.5
  L7: 7.4, 7.5
  L8: - 
  L9: Experiment 1, 9.2
  L10: 10.2, 
  L11: 11.2

---

(Conditioning, Stability, Floating Point)

9.
  W.
  1. 12: Condition of a Problem: Absolute Condition Number; Relative Condition Number; Examples 12.1-6
  2. 12: Condition of Matrix-Vector Multiplication; Theorem 12.1
  3. 12: Condition of Number of Matrix; Condition of a System of Equations; Theorem 12.2
  4. 13: Floating Point Numbers; Machine Epsilon
  E. 12.1

(Might have to do floating point numbers as a supplementary video)

10.
  W.
  1. 13: Floating Point Arithmetic; Fundamental Axiom of Floating Point Arithmetic
  2. 14: Algorithms; Accuracy;
  3. 14: Stability; Backward Stability;
  4. 14: The Meaning of O(\epsilon_machine); Dependence on m and n, not A and b
  E. 14.1

(Backward Error Analysis, Householder Triangularization, Backsubstitution)

11.
  W.
  1. 15: Stability of Floating Point Arithmetic; Examples 15.1-4
  2. 15: An unstable algorithm; Accuracy of a backward stable algorithm
  3. 15: Backward Error Analysis
  4. 16: QR Stability Experiment
  5. 16: Theorem 16.1
  E. 16.2

12.
  W.
  1. 16: Analyzing an Algorithm to Solve Ax=b; Algorithm 16.1
  2. 17: Triangular Systems; Algorithm 17.1 
  3. 17: Backward Stability Theorem
  4. 17: m = 1; m = 2; m = 3
  5. 17: General m
  E. 17.2

(Least Squares: Conditioning and Stability)

13.
  W.
  1. 18: Four conditioning problems; Theorem 18.1
  2. 18: Transformation to a Diagonal Matrix
  3. 18: Sensitivity of y to Perturbations in b; Sensitivity of x to Perturbations in b
  4. 18: Tilting the Range of A; Sensitivity of y to Perturbations in A; Sensitivity of x to Perturbations in A
  E. 18.4

14.
  W.
  1. 19: Example; Householder Triangularization
  2. 19: Gram-Schmidt Orthogonalization
  3. 19: Normal Equations
  4. 19: SVD; (Rank-Deficient Least Squares Problems)
  E. 19.1

HW

1. 12.3 ish
2. 14.2 + Higham approximation
3. 15.2
4. 16.1

--

15.
  W.
  1. LU Factorization; Example
  2. General Formulas and Two Strokes of Luck
  E. 20.1
  3. Instability of Gaussian Elimination without Pivoting
  4. Pivoting; Partial Pivoting

16.
  W.
  1. Partial Pivoting Example
  2. PA = LU Factorization and a Third Stroke of Luck
  3. Growth Factors; Worst Case Instability
  E. 22.1
  4. Stability in Practice; Explanation

17.
  W.
  1. Hermitian Positive Definite Matrices
  2. Symmetric Gaussian Elimination; Cholesky Factorization
  E. 23.1
  3. The Algorithm; Operation Count
  4. Stability; Solution of Ax = b
  
HW

1. 20.2
2. 21.2
3. 20.4
4. 21.6
5. 22.2
6. 22.4
7.
8.

--

18.
  W.
  1. Eigenvalue Problems
  2. Overview of eigenvalue algorithms
  3. Power iteration
  4.
  E. 24.1

19.
  W. 25.1.a 
  1. Reduction to Hessenberg or Tridiagonal Form
  2. Rayleigh Quotient
  3. Inverse Iteration
  4.
  E. 25.1.b

20.
  W. 25.3
  1. QR without shifts
  2.
  3.
  4.
  E. 28.1

21.
  W.
  1. QR with shifts, other eigenvalue algorithms
  2.
  3.
  4.
  E

22.
  W.
  1.
  2.
  3.
  4.
  E 31.3

HW

1. 24.2
3. 27.2
4. 27.3 (code)
6. 
7.
8. 31.3



