#! /usr/bin/env/python3
import json
import glob
import pathlib
import os
import nbconvert
import nbformat
import csv

name_to_sis = {
        # TODO: canvas identifier -> sis
    }

class Chdir:
  def __init__( self, newPath ):
    self.savedPath = os.getcwd()
    self.newPath = newPath

  def __enter__( self ):
      os.chdir( self.newPath )

  def __exit__( self, exc_type, exc_value, traceback ):
    os.chdir( self.savedPath )


def generate_report(notebook, **kwargs):
    """
    # Arguments

    - notebook: json representation of jupyter notebook

    # Returns

    - comment: string comment summarizing result

    - summary: json representation of jupyter notebook that extracts just the
      autograded cells and labels them PASSED or FAILED with the number of
      points
    """
    summary = notebook.copy()

    new_cells = []

    comment = []

    headings = kwargs.get("headings", []).copy()
    current_heading = headings.pop(0)
    current_count = 0
    current_total = 0
    current_possible = 0
    current_comment = ''
    if current_heading:
        new_cells.append({"cell_type": "markdown", "metadata": {}, "source": ["# Problem {}".format(current_heading[0])]})
        current_total = 0
        current_possible = 0
        current_comment = ''
        current_count = current_heading[1]

    for cell in notebook["cells"]:
        try:
            if cell["metadata"]["nbgrader"]["grade"] and cell["cell_type"] == "code" and not cell["metadata"]["nbgrader"]["solution"]:
                okay = True
                points = cell["metadata"]["nbgrader"]["points"]
                for output in cell["outputs"]:
                    if output["output_type"] == "error":
                        okay = False
                        break
                if okay:
                    new_cells.append({"cell_type": "markdown", "metadata": {}, "source": ["**PASSED {0}/{0}**".format(points)]})
                    current_total += points
                    current_possible += points
                    current_comment = current_comment + '  ok {0}/{0} \n'.format(points)
                    print("...Passed {0}/{0}".format(points))
                else:
                    new_cells.append({"cell_type": "markdown", "metadata": {}, "source": ["**FAILED 0/{0}**".format(points)]})
                    current_possible += points
                    current_comment = current_comment + '  not ok 0/{0} \n'.format(points)
                    print("...Failed 0/{0}".format(points))
                new_cells.append(cell)
                if current_heading:
                    current_count -= 1
                    if current_count <= 0:
                        comment.append(str(current_total))
                        comment.append(current_comment)
                        current_total = 0
                        current_possible = 0
                        current_comment = ''
                        try:
                            current_heading = headings.pop(0)
                        except:
                            current_heading = None
                        if current_heading:
                            new_cells.append({"cell_type": "markdown", "metadata": {}, "source": ["# Problem {}".format(current_heading[0])]})
                            current_count = current_heading[1]

        except KeyError: pass

    summary["cells"] = new_cells
    return summary, comment

def insert_reports(notebook):
    """
    Insert autograder reports into the notebook

    # Arguments

    - notebook: json representation of jupyter notebook

    """
    summary = notebook.copy()

    new_cells = []
    for cell in notebook["cells"]:
        new_cells.append(cell)
        try:
            if cell["metadata"]["nbgrader"]["grade"] and cell["metadata"]["nbgrader"]["cell_type"] == "code" and not cell["metadata"]["nbgrader"]["solution"]:
                okay = True
                points = cell["metadata"]["nbgrader"]["points"]
                for output in cell["outputs"]:
                    if output["output_type"] == "error":
                        okay = False
                        break
                if okay:
                    new_cells.append({"cell_type": "markdown", "metadata": {}, "source": ["**PASSED {0}/{0}**".format(points)]})
                    print("...Passed {0}/{0}".format(points))
                else:
                    new_cells.append({"cell_type": "markdown", "metadata": {}, "source": ["**FAILED 0/{0}**".format(points)]})
                    print("...Failed 0/{0}".format(points))
        except KeyError: pass

    summary["cells"] = new_cells
    return summary


headings = {
        'hw2': [
            ('2', 1),
            ('4.(a)', 3),
            ('4.(b)', 4),
            ('6', 5),
            ('7', 1),
            ('8.(b)', 3),
            ('8.(c)', 3),
            ],
        'hw3': [
            ('8.(a)', 3),
            ('8.(b)', 3),
            ],
        'hw4': [
            ('3', 10),
            ('5.(a)', 3),
            ('8.(a)', 6),
            ('8.(b)', 4),
            ],
        'hw5': [
            ('5.(a)', 5),
            ('5.(b)', 3),
            ('5.(c)', 1),
            ]
        }

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('assignment', type=pathlib.Path)
    parser.add_argument('--autograded-dir', type=pathlib.Path, default='autograded')
    args = parser.parse_args()
    print("Generating autograder reports for {}/**/{}".format(args.autograded_dir, args.assignment))
    directories = glob.glob("{}/**/{}".format(args.autograded_dir,args.assignment))
    pdf_exporter = nbconvert.exporters.PDFExporter()
    heading = headings.get(str(args.assignment), [])
    csv_fields = ['Student Name']
    for h in heading:
        csv_fields.append('Q ' + h[0])
        csv_fields.append('Q ' + h[0] + ', comment')
    rows = {}
    for name in name_to_sis.keys():
        sis = name_to_sis[name]
        row = [name, 'Assignment could not be autograded']
        rows[sis] = row
    for dirt in directories:
        student = str(dirt).split('/')[1]
        sis = name_to_sis[student]
        row = [student]
        with Chdir(dirt):
            print(".In {}".format(os.getcwd()))
            notebooks = glob.glob("*.ipynb")
            for notebook in notebooks:
                print("..Reading {}".format(notebook))
                with open(notebook) as raw:
                    nb = json.load(raw)
                print("..Generating a report for {}".format(notebook))
                summarynb, comment = generate_report(nb, headings=heading)
                #print("..Comment: {}".format(comment))
                row.extend(comment)
                #summarynbf = nbformat.from_dict(summarynb)
                #print("..Converting report to pdf")
                #pdf = nbconvert.exporters.export(pdf_exporter, summarynbf)
                #summaryname = str(pathlib.Path(notebook).with_suffix('')) + '_summary.pdf'
                #print("..Writing report to {}".format(summaryname))
                #with open(summaryname, "wb") as file:
                #    file.write(pdf[0])
        rows[sis] = row
    comment_file = pathlib.Path('{}-comments.csv'.format(args.assignment))
    with open(comment_file, 'w') as f:
        csvwriter = csv.writer(f)
        csvwriter.writerow(csv_fields)
        csvwriter.writerows(rows.values())

